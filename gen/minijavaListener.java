// Generated from D:/siit/PP/mini-java-parser/src/main/antlr\minijava.g4 by ANTLR 4.10.1
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link minijavaParser}.
 */
public interface minijavaListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link minijavaParser#text}.
	 * @param ctx the parse tree
	 */
	void enterText(minijavaParser.TextContext ctx);
	/**
	 * Exit a parse tree produced by {@link minijavaParser#text}.
	 * @param ctx the parse tree
	 */
	void exitText(minijavaParser.TextContext ctx);
	/**
	 * Enter a parse tree produced by {@link minijavaParser#class}.
	 * @param ctx the parse tree
	 */
	void enterClass(minijavaParser.ClassContext ctx);
	/**
	 * Exit a parse tree produced by {@link minijavaParser#class}.
	 * @param ctx the parse tree
	 */
	void exitClass(minijavaParser.ClassContext ctx);
	/**
	 * Enter a parse tree produced by {@link minijavaParser#class_body}.
	 * @param ctx the parse tree
	 */
	void enterClass_body(minijavaParser.Class_bodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link minijavaParser#class_body}.
	 * @param ctx the parse tree
	 */
	void exitClass_body(minijavaParser.Class_bodyContext ctx);
	/**
	 * Enter a parse tree produced by {@link minijavaParser#attributes_list}.
	 * @param ctx the parse tree
	 */
	void enterAttributes_list(minijavaParser.Attributes_listContext ctx);
	/**
	 * Exit a parse tree produced by {@link minijavaParser#attributes_list}.
	 * @param ctx the parse tree
	 */
	void exitAttributes_list(minijavaParser.Attributes_listContext ctx);
	/**
	 * Enter a parse tree produced by {@link minijavaParser#attributes}.
	 * @param ctx the parse tree
	 */
	void enterAttributes(minijavaParser.AttributesContext ctx);
	/**
	 * Exit a parse tree produced by {@link minijavaParser#attributes}.
	 * @param ctx the parse tree
	 */
	void exitAttributes(minijavaParser.AttributesContext ctx);
	/**
	 * Enter a parse tree produced by {@link minijavaParser#atribute}.
	 * @param ctx the parse tree
	 */
	void enterAtribute(minijavaParser.AtributeContext ctx);
	/**
	 * Exit a parse tree produced by {@link minijavaParser#atribute}.
	 * @param ctx the parse tree
	 */
	void exitAtribute(minijavaParser.AtributeContext ctx);
	/**
	 * Enter a parse tree produced by {@link minijavaParser#instantiation}.
	 * @param ctx the parse tree
	 */
	void enterInstantiation(minijavaParser.InstantiationContext ctx);
	/**
	 * Exit a parse tree produced by {@link minijavaParser#instantiation}.
	 * @param ctx the parse tree
	 */
	void exitInstantiation(minijavaParser.InstantiationContext ctx);
	/**
	 * Enter a parse tree produced by {@link minijavaParser#function_list}.
	 * @param ctx the parse tree
	 */
	void enterFunction_list(minijavaParser.Function_listContext ctx);
	/**
	 * Exit a parse tree produced by {@link minijavaParser#function_list}.
	 * @param ctx the parse tree
	 */
	void exitFunction_list(minijavaParser.Function_listContext ctx);
	/**
	 * Enter a parse tree produced by {@link minijavaParser#function}.
	 * @param ctx the parse tree
	 */
	void enterFunction(minijavaParser.FunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link minijavaParser#function}.
	 * @param ctx the parse tree
	 */
	void exitFunction(minijavaParser.FunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link minijavaParser#parameter}.
	 * @param ctx the parse tree
	 */
	void enterParameter(minijavaParser.ParameterContext ctx);
	/**
	 * Exit a parse tree produced by {@link minijavaParser#parameter}.
	 * @param ctx the parse tree
	 */
	void exitParameter(minijavaParser.ParameterContext ctx);
	/**
	 * Enter a parse tree produced by {@link minijavaParser#body}.
	 * @param ctx the parse tree
	 */
	void enterBody(minijavaParser.BodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link minijavaParser#body}.
	 * @param ctx the parse tree
	 */
	void exitBody(minijavaParser.BodyContext ctx);
	/**
	 * Enter a parse tree produced by {@link minijavaParser#variable_list}.
	 * @param ctx the parse tree
	 */
	void enterVariable_list(minijavaParser.Variable_listContext ctx);
	/**
	 * Exit a parse tree produced by {@link minijavaParser#variable_list}.
	 * @param ctx the parse tree
	 */
	void exitVariable_list(minijavaParser.Variable_listContext ctx);
	/**
	 * Enter a parse tree produced by {@link minijavaParser#variable}.
	 * @param ctx the parse tree
	 */
	void enterVariable(minijavaParser.VariableContext ctx);
	/**
	 * Exit a parse tree produced by {@link minijavaParser#variable}.
	 * @param ctx the parse tree
	 */
	void exitVariable(minijavaParser.VariableContext ctx);
	/**
	 * Enter a parse tree produced by {@link minijavaParser#statement_list}.
	 * @param ctx the parse tree
	 */
	void enterStatement_list(minijavaParser.Statement_listContext ctx);
	/**
	 * Exit a parse tree produced by {@link minijavaParser#statement_list}.
	 * @param ctx the parse tree
	 */
	void exitStatement_list(minijavaParser.Statement_listContext ctx);
	/**
	 * Enter a parse tree produced by {@link minijavaParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterStatement(minijavaParser.StatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link minijavaParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitStatement(minijavaParser.StatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link minijavaParser#compound_statement}.
	 * @param ctx the parse tree
	 */
	void enterCompound_statement(minijavaParser.Compound_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link minijavaParser#compound_statement}.
	 * @param ctx the parse tree
	 */
	void exitCompound_statement(minijavaParser.Compound_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link minijavaParser#assignment_statement}.
	 * @param ctx the parse tree
	 */
	void enterAssignment_statement(minijavaParser.Assignment_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link minijavaParser#assignment_statement}.
	 * @param ctx the parse tree
	 */
	void exitAssignment_statement(minijavaParser.Assignment_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link minijavaParser#num_exp}.
	 * @param ctx the parse tree
	 */
	void enterNum_exp(minijavaParser.Num_expContext ctx);
	/**
	 * Exit a parse tree produced by {@link minijavaParser#num_exp}.
	 * @param ctx the parse tree
	 */
	void exitNum_exp(minijavaParser.Num_expContext ctx);
	/**
	 * Enter a parse tree produced by {@link minijavaParser#exp}.
	 * @param ctx the parse tree
	 */
	void enterExp(minijavaParser.ExpContext ctx);
	/**
	 * Exit a parse tree produced by {@link minijavaParser#exp}.
	 * @param ctx the parse tree
	 */
	void exitExp(minijavaParser.ExpContext ctx);
	/**
	 * Enter a parse tree produced by {@link minijavaParser#literal}.
	 * @param ctx the parse tree
	 */
	void enterLiteral(minijavaParser.LiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link minijavaParser#literal}.
	 * @param ctx the parse tree
	 */
	void exitLiteral(minijavaParser.LiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link minijavaParser#function_call}.
	 * @param ctx the parse tree
	 */
	void enterFunction_call(minijavaParser.Function_callContext ctx);
	/**
	 * Exit a parse tree produced by {@link minijavaParser#function_call}.
	 * @param ctx the parse tree
	 */
	void exitFunction_call(minijavaParser.Function_callContext ctx);
	/**
	 * Enter a parse tree produced by {@link minijavaParser#argument}.
	 * @param ctx the parse tree
	 */
	void enterArgument(minijavaParser.ArgumentContext ctx);
	/**
	 * Exit a parse tree produced by {@link minijavaParser#argument}.
	 * @param ctx the parse tree
	 */
	void exitArgument(minijavaParser.ArgumentContext ctx);
	/**
	 * Enter a parse tree produced by {@link minijavaParser#if_statement}.
	 * @param ctx the parse tree
	 */
	void enterIf_statement(minijavaParser.If_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link minijavaParser#if_statement}.
	 * @param ctx the parse tree
	 */
	void exitIf_statement(minijavaParser.If_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link minijavaParser#if_part}.
	 * @param ctx the parse tree
	 */
	void enterIf_part(minijavaParser.If_partContext ctx);
	/**
	 * Exit a parse tree produced by {@link minijavaParser#if_part}.
	 * @param ctx the parse tree
	 */
	void exitIf_part(minijavaParser.If_partContext ctx);
	/**
	 * Enter a parse tree produced by {@link minijavaParser#rel_exp}.
	 * @param ctx the parse tree
	 */
	void enterRel_exp(minijavaParser.Rel_expContext ctx);
	/**
	 * Exit a parse tree produced by {@link minijavaParser#rel_exp}.
	 * @param ctx the parse tree
	 */
	void exitRel_exp(minijavaParser.Rel_expContext ctx);
	/**
	 * Enter a parse tree produced by {@link minijavaParser#return_statement}.
	 * @param ctx the parse tree
	 */
	void enterReturn_statement(minijavaParser.Return_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link minijavaParser#return_statement}.
	 * @param ctx the parse tree
	 */
	void exitReturn_statement(minijavaParser.Return_statementContext ctx);
}