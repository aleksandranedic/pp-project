// Generated from D:/siit/PP/mini-java-parser/src/main/antlr\minijava.g4 by ANTLR 4.10.1
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link minijavaParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface minijavaVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link minijavaParser#text}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitText(minijavaParser.TextContext ctx);
	/**
	 * Visit a parse tree produced by {@link minijavaParser#class}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClass(minijavaParser.ClassContext ctx);
	/**
	 * Visit a parse tree produced by {@link minijavaParser#class_body}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClass_body(minijavaParser.Class_bodyContext ctx);
	/**
	 * Visit a parse tree produced by {@link minijavaParser#attributes_list}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAttributes_list(minijavaParser.Attributes_listContext ctx);
	/**
	 * Visit a parse tree produced by {@link minijavaParser#attributes}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAttributes(minijavaParser.AttributesContext ctx);
	/**
	 * Visit a parse tree produced by {@link minijavaParser#atribute}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAtribute(minijavaParser.AtributeContext ctx);
	/**
	 * Visit a parse tree produced by {@link minijavaParser#instantiation}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInstantiation(minijavaParser.InstantiationContext ctx);
	/**
	 * Visit a parse tree produced by {@link minijavaParser#function_list}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunction_list(minijavaParser.Function_listContext ctx);
	/**
	 * Visit a parse tree produced by {@link minijavaParser#function}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunction(minijavaParser.FunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link minijavaParser#parameter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParameter(minijavaParser.ParameterContext ctx);
	/**
	 * Visit a parse tree produced by {@link minijavaParser#body}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBody(minijavaParser.BodyContext ctx);
	/**
	 * Visit a parse tree produced by {@link minijavaParser#variable_list}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariable_list(minijavaParser.Variable_listContext ctx);
	/**
	 * Visit a parse tree produced by {@link minijavaParser#variable}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariable(minijavaParser.VariableContext ctx);
	/**
	 * Visit a parse tree produced by {@link minijavaParser#statement_list}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatement_list(minijavaParser.Statement_listContext ctx);
	/**
	 * Visit a parse tree produced by {@link minijavaParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatement(minijavaParser.StatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link minijavaParser#compound_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCompound_statement(minijavaParser.Compound_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link minijavaParser#assignment_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssignment_statement(minijavaParser.Assignment_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link minijavaParser#num_exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNum_exp(minijavaParser.Num_expContext ctx);
	/**
	 * Visit a parse tree produced by {@link minijavaParser#exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExp(minijavaParser.ExpContext ctx);
	/**
	 * Visit a parse tree produced by {@link minijavaParser#literal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLiteral(minijavaParser.LiteralContext ctx);
	/**
	 * Visit a parse tree produced by {@link minijavaParser#function_call}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunction_call(minijavaParser.Function_callContext ctx);
	/**
	 * Visit a parse tree produced by {@link minijavaParser#argument}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArgument(minijavaParser.ArgumentContext ctx);
	/**
	 * Visit a parse tree produced by {@link minijavaParser#if_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIf_statement(minijavaParser.If_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link minijavaParser#if_part}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIf_part(minijavaParser.If_partContext ctx);
	/**
	 * Visit a parse tree produced by {@link minijavaParser#rel_exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRel_exp(minijavaParser.Rel_expContext ctx);
	/**
	 * Visit a parse tree produced by {@link minijavaParser#return_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReturn_statement(minijavaParser.Return_statementContext ctx);
}