import org.antlr.v4.runtime.tree.TerminalNode;

import java.util.ArrayList;

public class SemanticAnalysis extends minijavaBaseListener{
    /*SEMANTICKE GRESKE:
    1. KLASE MORAJU IMATI JEDINSTVENI NAZIV
    2. NAZIVI ATRIBUTA I PROMENLJIVIH UNUTAR KLASE I NAZIVI PROMENLJIVIH UNUTAR FUNKCIJA MORAJU BITI JEDINSTVENI
    3. NAZIVI FUNCKIJA UNUTAR KLASE MORAJU BITI JEDINSTVENI
    4. PROMENLJIVA MORA BITI DEKLARISANA PRE NEGO STO BUDE KORISCENA
    5. PRILIKOM OPERACIJE DODELE TIPOVI SA LEVE I DESNE STRANE JEDNAKOSTI SE MORAJU POKLAPATI
    6. POVRATNA VREDNOST FUNKCIJE MORA BITI ODGOVARAJUCA
    7. POZIV FUNKCIJE MORA POZIVATI DEKLARISANU FUNKCIJU
     */

    ArrayList<String> classNames = new ArrayList<>();
    ArrayList<String> classAttributes = new ArrayList<>();
    ArrayList<Variable> attributeVariables = new ArrayList<>();
    ArrayList<String> functionNames = new ArrayList<>();
    ArrayList<Variable> functionReturnTypes = new ArrayList<>();
    ArrayList<Variable> functionVariables = new ArrayList<>();
    String assignmentType;
    String currentFunction;

    @Override
    public void enterClass(minijavaParser.ClassContext ctx) {
        TerminalNode node = ctx.ID();
        String className = node.getText();
        if (classNames.contains(className)){
            System.out.println("Class " + className + " already defined.");
        }
        else{
            classNames.add(className);
        }
    }

    @Override
    public void exitClass(minijavaParser.ClassContext ctx) {
        functionNames.clear();
        functionReturnTypes.clear();
        classAttributes.clear();
        attributeVariables.clear();
    }

    @Override
    public void enterFunction(minijavaParser.FunctionContext ctx) {
        TerminalNode node = ctx.ID();
        String functionName = node.getText();
        currentFunction = functionName;
        if (functionNames.contains(functionName)){
            System.out.println("Function " + functionName + " inside class already defined.");
        }
        else if (classAttributes.contains(functionName)){
            System.out.println("Attribute with name " + functionName + " already exists.");
        }
        else{
            functionNames.add(functionName);
            functionReturnTypes.add(new Variable(ctx.TYPE().getText(), functionName));
        }
    }
    @Override
    public void enterFunction_call(minijavaParser.Function_callContext ctx) {
        TerminalNode node = ctx.ID();
        String functionName = node.getText();
        if (!functionNames.contains(functionName)){
            System.out.println("Function " + functionName + " must be defined first.");
        }
        assignmentType = getTypeOfFunction(functionName);
    }
    @Override public void enterExp(minijavaParser.ExpContext ctx) {
        var litNode = ctx.literal();
        if (litNode != null){
            TerminalNode typeNode = (TerminalNode) ctx.literal().getChild(0);
            switch(typeNode.getSymbol().getType()) {
                case minijavaLexer.INT_NUMBER: {assignmentType = "int"; break;}
                case minijavaLexer.UINT_NUMBER: assignmentType = "unsigned";
            }
        }
        TerminalNode node = ctx.ID();
        if (node != null){
            String identifier = node.getText();
            Variable var = getVariable(identifier);
            Variable var1 = getVariableAttr(identifier);
            if (var != null){
                assignmentType = var.getType();
            }
            else if (var1 != null){
                assignmentType = var1.getType();
            }
            else
                System.out.println("Variable on the right side of the assignment must be declared.");
        }
    }
    @Override
    public void enterInstantiation(minijavaParser.InstantiationContext ctx) {
        TerminalNode node = ctx.ID().get(0);
        String className = node.getText();
        TerminalNode node1 = ctx.ID().get(2);
        String classConstructor = node1.getText();
        if (!classNames.contains(className)){
            System.out.println("Class " + className + " must be defined first.");
        }
        else if (!classConstructor.equals(className)){
            System.out.println("Variable type " + className + " and constructor don't match.");
        }
    }
    @Override public void enterAtribute(minijavaParser.AtributeContext ctx) {
        TerminalNode node = ctx.ID();
        String attributeName = node.getText();
        if (classAttributes.contains(attributeName)) {
            System.out.println("Attribute with name " + attributeName + " already declared.");
        }
        else {
            classAttributes.add(attributeName);
            attributeVariables.add(new Variable(ctx.TYPE().getText(), attributeName));
        }
    }
    @Override public void enterVariable(minijavaParser.VariableContext ctx) {
        TerminalNode type = ctx.TYPE();
        String typeName = type.getText();
        TerminalNode variable = ctx.ID();
        String variableName = variable.getText();
        if (containsVariable(variableName))
            System.out.println("Variable " + variableName + " already defined.");
        else{
            functionVariables.add(new Variable(typeName, variableName));
        }
    }
    @Override public void enterAssignment_statement(minijavaParser.Assignment_statementContext ctx) {
        TerminalNode node = ctx.ID();
        String variable = node.getText();
        if (!containsVariable(variable) && !classAttributes.contains(variable))
            System.out.println("Variable " + variable + " must be declared first.");
    }
    @Override public void exitAssignment_statement(minijavaParser.Assignment_statementContext ctx) {
        TerminalNode node = ctx.ID();
        String variable = node.getText();
        Variable var = getVariable(variable);
        if (var!= null){
            if (!var.getType().equals(assignmentType)) {
                System.out.println("Variable " + variable + " with type " + var.getType() + " must be the compatible with right side of assignment statement.");
            }
        }
        var = getVariableAttr(variable);
        if (var!= null){
            if (!var.getType().equals(assignmentType)) {
                System.out.println("Attribute " + variable + " with type " + var.getType() + " must be the compatible with right side of assignment statement.");
            }
        }
    }
    @Override public void exitFunction(minijavaParser.FunctionContext ctx) {
        functionVariables.clear();
    }
    @Override public void exitReturn_statement(minijavaParser.Return_statementContext ctx) {
        String funcType = getTypeOfFunction(currentFunction);
        if (!funcType.equals(assignmentType))
            System.out.println("Incompatible return type in function " + currentFunction + ".");
    }
    private boolean containsVariable(String varName){
        for (Variable var : functionVariables){
            if (var.getName().equals(varName))
                return true;
        }
        return false;
    }
    private Variable getVariable(String varName){
        for (Variable var : functionVariables){
            if (var.getName().equals(varName))
                return var;
        }
        return null;
    }
    private Variable getVariableAttr(String varName){
        for (Variable var : attributeVariables){
            if (var.getName().equals(varName))
                return var;
        }
        return null;
    }
    private String getTypeOfFunction(String varName){
        for (Variable var : functionReturnTypes){
            if (var.getName().equals(varName))
                return var.getType();
        }
        return "";
    }
}
