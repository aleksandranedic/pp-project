import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.antlr.v4.gui.TreeViewer;

import javax.swing.*;
import java.awt.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        File dir = new File("./src/main/java/tests");
        File[] directoryListing = dir.listFiles();
        if (directoryListing != null) {
            for (File child : directoryListing) {
                try {
                    String text = Files.readString(child.toPath(), StandardCharsets.US_ASCII);
                    String title = (child.toPath().getName(child.toPath().getNameCount()-1)).toString();
                    System.out.println("*****" + title + "*****\n" + text + "\n" );
                    minijavaLexer lexer = new minijavaLexer(CharStreams.fromString(text));
                    CommonTokenStream tokens = new CommonTokenStream(lexer);

                    minijavaParser parser = new minijavaParser(tokens);
                    ParseTree tree = parser.text();
                    ParseTreeWalker walker = new ParseTreeWalker();
                    SemanticAnalysis listener = new SemanticAnalysis();
                    walker.walk(listener, tree);

                    JFrame frame = new JFrame(title);
                    JPanel panel = new JPanel();
                    TreeViewer viewer = new TreeViewer(Arrays.asList(parser.getRuleNames()), tree);
                    viewer.setScale(1.5);
                    panel.add(viewer);

                    JScrollPane scrPane = new JScrollPane(panel);
                    scrPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
                    scrPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);

                    frame.add(scrPane);
                    frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                    frame.pack();
                    frame.setVisible(true);

                    System.out.println("Pritisnite neki taster da nastavite sa radom.");
                    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
                    reader.readLine();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
