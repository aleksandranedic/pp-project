Projekat: miniJava u ANTLR alatu

Za pokretanje aplikacije potrebno je imati IntelliJ IDE instaliran na računaru.

Pored toga, potrebno je imati instalirani:

Maven v3.6.3.

Java jdk v17

Aplikaciju možete pokrenuti klikom na start dugme u IntelliJ IDE.


Autor: Nedić Aleksandra